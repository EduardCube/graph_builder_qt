#ifndef GRAPHWIDGET_HPP
#define GRAPHWIDGET_HPP

#include <QWidget>
#include <QRegExp>
#include <QMessageBox>
#include <QDir>
#include <QColorDialog>
#include <QColor>
#include <QFileDialog>
#include "graph.hpp"

namespace Ui {
class GraphWidget;
}

class GraphWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GraphWidget(const QStringList &titles={},QTableWidget* table=nullptr,bool isDirected=false);
    ~GraphWidget();

private:
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void scaleView(qreal scaleFactor);
    void keyPressEvent(QKeyEvent *event) override;


    Ui::GraphWidget *ui;
    QGraphicsScene *scene;
    Graph *graph_;
    QStringList rgb_;
    QList<Node *> nodes_;
    QList<Edge *> edges_;
private slots:
    void OkColorEnabled();
    void zoomIn();
    void zoomOut();
    void on_btnOkColor_released();
    void on_rbGraphDirected_clicked();
    void on_rbGraphUndirected_clicked();
    void on_btnSave_released();
    void on_pushButton_3_released();
    void on_pushButton_2_released();
    void on_pushButton_released();
    void on_pushButton_4_released();
};

#endif // GRAPHWIDGET_HPP
