#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <QObject>
#include <QList>
#include <QTableWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QLayout>
#include "node.hpp"
#include "edge.hpp"

class Graph
{
public:
    Graph(QGraphicsView *parent = nullptr,const QStringList &titles={},QTableWidget* table=nullptr,bool isDirected=false);
    QList<Node*> getNodes() const;
    QList<Edge*> getEdges() const;

private:
    size_t getRandom(int ,int );
    QGraphicsView view_;
    QList<Node*> nodes_;
    QList<Edge*> edges_;
    QStringList titles_;

};

#endif // GRAPH_HPP
