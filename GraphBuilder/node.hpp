#ifndef NODE_HPP
#define NODE_HPP

#include <QObject>
#include<QGraphicsView>
#include<QGraphicsItem>
#include<QColor>
#include "edge.hpp"

class Edge;
class Node : public QGraphicsItem
{

public:
    explicit Node(QGraphicsView *parent = nullptr,QString title="");
    virtual ~Node()override{}
private:


    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;


     QRectF boundingRect() const override;
     QPainterPath shape() const override;
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

     void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
     void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

     QGraphicsView view_;
     QPointF newPosition_;
     QColor nodeColor_;
     QColor textColor_;
     QString title_;
     QList<Edge *> edgeList_;
public:
    void addEdge(Edge *edge);
    void setNodeColor(short ,short,short);
    void setNodeColor(QColor&);
    void setTextColor(QColor&);
};

#endif // NODE_HPP
