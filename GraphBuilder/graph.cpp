#include "graph.hpp"



Graph::Graph(QGraphicsView *parent, const QStringList &titles,QTableWidget* table,bool isDirected):view_(parent),titles_(titles)
{
    size_t x,y;
    x=110; // range
    y=540;
    for(auto it: titles_)
    {
        Node *node= new Node(parent,it);
        node->setPos(getRandom(x, y), getRandom(x, y));//create nodes in random place
        nodes_.push_back(node);
    }
    size_t count=table->rowCount();

    for(int row = 0;row <count; row++)
    {
        for(int column = 0;column< count ;column++)
        {       
            // Taking the widget from the cell
                   QWidget *item = ( table->cellWidget(row,column));
                   // Taking the widget from the layout and cast it to QCheckBox
                   QCheckBox *check = qobject_cast <QCheckBox*> (item->layout()->itemAt(0)->widget());
                if(check->isChecked())
                {
                    Edge* edge=new Edge(nodes_[row],nodes_[column],isDirected);
                    edges_.push_back(edge);
                }        

        }

    }
}

QList<Node *> Graph::getNodes() const
{
    return nodes_;
}

QList<Edge *> Graph::getEdges() const
{
    return edges_;
}

size_t Graph::getRandom(int low, int hight)
{
    //srand(time(NULL));
    return (qrand() % ((hight + 1) - low) + low);
}
