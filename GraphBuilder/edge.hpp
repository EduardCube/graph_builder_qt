#ifndef EDGE_HPP
#define EDGE_HPP


#include <QGraphicsItem>
#include "node.hpp"
#include <qmath.h>
#include <QPainter>

class Node;

class Edge : public QGraphicsItem
{
public:
    Edge(Node *sourceNode, Node *destNode,bool isDirected=false);

    void adjust();
    Node *sourceNode() const;
    Node *destNode() const;
    void setDirection(bool);

private:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void drawArrows(QPainter *painter,QLineF &line);

    bool isDirected_;
    Node *source_, *destination_;
    QPointF sourcePoint_;
    QPointF destinationPoint_;
    qreal arrowSize_;
};


#endif // EDGE_HPP
