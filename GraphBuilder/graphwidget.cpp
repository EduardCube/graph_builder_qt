#include "graphwidget.hpp"
#include "ui_graphwidget.h"

GraphWidget::GraphWidget(const QStringList &titles,QTableWidget* table,bool isDirected) :
    ui(new Ui::GraphWidget)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);   // Init graphics scene
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    scene->setSceneRect(50,50,540,540);
    ui->gvGraph->setScene(scene);
    ui->gvGraph->setCacheMode(QGraphicsView::CacheBackground);
    ui->gvGraph->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    ui->gvGraph->setRenderHint(QPainter::Antialiasing);
    ui->gvGraph->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);


    QRegExp ColorExpr("(([0-9]{1}|[1-9]{1}[0-9]{1}|[1]{1}[0]{1}[0-9]{1}|[1]{1}[1-9]{1}[0-9]{1}|[2]{1}[0]{1}[0-9]{1}|[2]{1}[1-4]{1}[0-9]{1}|[2]{1}[5]{1}[0-5]{1})(,)){2}"
                      "([0-9]{1}|[1-9]{1}[0-9]{1}|[1]{1}[0]{1}[0-9]{1}|[1]{1}[1-9]{1}[0-9]{1}|[2]{1}[0]{1}[0-9]{1}|[2]{1}[1-4]{1}[0-9]{1}|[2]{1}[5]{1}[0-5]{1})");// for rgb

    ui->leColor->setValidator(new QRegExpValidator(ColorExpr,this));
    connect(ui->leColor,SIGNAL(textChanget(QString)),this,SLOT(OkColorEnabled())); // set validator rgb


    graph_=new Graph(ui->gvGraph,titles,table,isDirected);
     nodes_=graph_->getNodes();
     edges_=graph_->getEdges();
    for(auto it: nodes_)
    {
        scene->addItem(it);
    }
    for(auto it: edges_)
    {
        scene->addItem(it);
    }
}

GraphWidget::~GraphWidget()
{
    delete ui;
}

void GraphWidget::OkColorEnabled()
{
    ui->btnOkColor->setEnabled(ui->leColor->hasAcceptableInput());
}

void GraphWidget::on_btnOkColor_released()
{
    QString str =ui->leColor->text();
    rgb_=str.split(',');
    if(rgb_.size()==2)
        QMessageBox::warning(this,"Warning","You did not specify a blue color");
    else if(rgb_.size()==1)
        QMessageBox::warning(this,"Warning","You did not specify a green and blue color");
    else if(rgb_.size()==0)
         QMessageBox::warning(this,"Warning","You did not specify a red, green and blue color");
    else
    {
        short red; short green; short blue;
        red=rgb_[0].toShort();
        green=rgb_[1].toShort();
        blue=rgb_[2].toShort();

        for(auto it: nodes_)
        {
            it->setNodeColor(red,green,blue);
        }
    }
}

void GraphWidget::on_rbGraphDirected_clicked()
{
     edges_=graph_->getEdges();
     for(auto it: edges_)
     {
         it->setDirection(true);
     }
}

void GraphWidget::on_rbGraphUndirected_clicked()
{
    edges_=graph_->getEdges();
    for(auto it: edges_)
    {
        it->setDirection(false);
    }
}

void GraphWidget::on_btnSave_released()
{

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save image"), "",tr("BMP (*.BMP);;JPG (*.JPG);;PNG (*.PNG)"));
    if(fileName=="")
        QMessageBox::warning(this,"Warning","Write a file name");
    else
    {
        QPixmap pixMap = QPixmap::grabWidget(ui->gvGraph);
        if(pixMap.save(fileName))
            QMessageBox::warning(this,"Warning","Saved");
    }
}
#if QT_CONFIG(wheelevent)
void GraphWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow((double)2, -event->delta() / 240.0));
}
#endif

void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = ui->gvGraph->transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;

    ui->gvGraph->scale(scaleFactor, scaleFactor);
}
void GraphWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {

    case Qt::Key_Plus:
        zoomIn();
        break;
    case Qt::Key_Minus:
        zoomOut();

    }
}
void GraphWidget::zoomIn()
{
    scaleView(qreal(1.2));
}

void GraphWidget::zoomOut()
{
    scaleView(1 / qreal(1.2));
}

void GraphWidget::on_pushButton_3_released()
{
    zoomIn();
}

void GraphWidget::on_pushButton_2_released()
{
    zoomOut();
}

void GraphWidget::on_pushButton_released()
{
    QColor color=QColorDialog::getColor(Qt::white,this,"Choose color");
    if(color.isValid())
    {
        for(auto it: nodes_)
        {
            it->setNodeColor(color);
        }
    }
}

void GraphWidget::on_pushButton_4_released()
{
    QColor color=QColorDialog::getColor(Qt::white,this,"Choose color");
    if(color.isValid())
    {
        for(auto it: nodes_)
        {
            it->setTextColor(color);
        }
    }
}
