#include "node.hpp"



Node::Node(QGraphicsView *parent, QString title) :view_(parent), nodeColor_(100,224,255),textColor_(Qt::black),title_(title)
{
        setFlag(ItemIsMovable);
        setFlag(ItemSendsGeometryChanges);
        setZValue(-1);   //To ensure that the nodes are always stacked on top of edges
}

QRectF Node::boundingRect() const
{
    return QRectF (-30,-30,60,60);
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-30,-30,60,60);
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(textColor_);            // set pen
    painter->setBrush(nodeColor_);             // set color
    painter->drawEllipse(-30,-30,60,60);   // draw ellipse
    QFont font = painter->font();
    font.setPixelSize(15);
    painter->setFont(font);
    painter->drawText(boundingRect(),Qt::AlignCenter,title_); // set text
    Q_UNUSED(option);
    Q_UNUSED(widget);
}


void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

void Node::setNodeColor(short red, short green, short blue) // user color
{
    nodeColor_.setRed(red);
    nodeColor_.setGreen(green);
    nodeColor_.setBlue(blue);
    update();
}

void Node::setNodeColor(QColor &color)
{
    nodeColor_=color;
    update();
}

void Node::addEdge(Edge *edge)
{
    edgeList_ << edge;
    edge->adjust();
}

void Node::setTextColor(QColor &color)
{
    textColor_=color;
    update();
}
QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        foreach (Edge *edge, edgeList_)
            edge->adjust();
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}



