#include "mainwindow.hpp"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    goodVerticesNumber_=false;
    QRegExp verticesExpr("([a-zA-Z0-9]{1,10}(,)){1,10}");
    ui->leMVertices->setValidator(new QRegExpValidator(verticesExpr,this));

    AddImageHint();

}

MainWindow::~MainWindow()
{
    delete graphPropeties_;
    delete ui;
}


void MainWindow::AddImageHint()
{
    QPixmap graphHint(":/resources/Images/ZAeukvIl3pg.jpg");
    ui->lblGraphHint->setPixmap(graphHint.scaled(450,263,Qt::KeepAspectRatio));
}

void MainWindow::SetTableHeaders(QStringList & titleList)
{
     size_t count = titleList.size();
     size_t i;
     if(count<rowAndColumnCount_)
     {
        for(i=count+1;i<rowAndColumnCount_+1;i++)
            titleList.push_back(QString::number(i));
     }
     ui->twGraphMatrix->setHorizontalHeaderLabels(titleList);
     ui->twGraphMatrix->setVerticalHeaderLabels(titleList);
}

void MainWindow::CreateTable()
{
     rowAndColumnCount_ = ui->sbVerticesNum->value();
     ui->twGraphMatrix->setRowCount(rowAndColumnCount_);
     ui->twGraphMatrix->setColumnCount(rowAndColumnCount_);
    for(size_t row=0;row<rowAndColumnCount_;row++)
    {
        for(size_t column=0;column<rowAndColumnCount_;column++)
        {
            ui->twGraphMatrix->setItem(row, column, new QTableWidgetItem(""));

        }
    }
     setCellCheckBox(ui->twGraphMatrix);


}

void MainWindow::setCellCheckBox(QTableWidget * table)
{

    for(size_t i=0;i<rowAndColumnCount_;i++)
    {
        for(size_t j=0;j<rowAndColumnCount_;j++)
        {
            QWidget *checkBoxWidget = new QWidget();
            QCheckBox *checkBox = new QCheckBox();      // We declare and initialize the checkbox
            QHBoxLayout *layoutCheckBox = new QHBoxLayout(checkBoxWidget); // create a layer with reference to the widget
            layoutCheckBox->addWidget(checkBox);            // Set the checkbox in the layer
            layoutCheckBox->setAlignment(Qt::AlignCenter);  // Center the checkbox
            layoutCheckBox->setContentsMargins(0,0,0,0);    // Set the zero padding
            ui->twGraphMatrix->setCellWidget(i, j, checkBoxWidget);
        }
    }

}

void MainWindow::on_btnFCreate_released()
{
    bool isDirected;
    if(ui->rbtnMDirected->isChecked())
        isDirected=true;
    else isDirected=false;
    graphPropeties_=new GraphWidget(titles_,ui->twGraphMatrix,isDirected);
    graphPropeties_->show();
}

void MainWindow::on_btnMCreate_released()
{
    on_btnFCreate_released();
}
void MainWindow::on_btnGraphTable_clicked()
{}

void MainWindow::on_btnVerticesNumber_clicked()
{}

void MainWindow::on_btnTableCreate_clicked()
{
    if(ui->sbVerticesNum->value()!=0)
    goodVerticesNumber_=true;
    else QMessageBox::warning(this,"Warning","Can't be zero");
    QString str =ui->leMVertices->text();
     titles_ = str.split(',');

   CreateTable();
   SetTableHeaders(titles_);
}


