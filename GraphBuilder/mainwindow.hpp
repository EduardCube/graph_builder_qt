#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include<QPixmap>
#include <QTabWidget>
#include <QMessageBox>
#include <QRegExp>
#include <QTableWidget>
#include <QCheckBox>
#include "graphwidget.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:



    void on_btnGraphTable_clicked();

    void on_btnFCreate_released();

    void on_btnMCreate_released();

    void on_btnVerticesNumber_clicked();

    void on_btnTableCreate_clicked();

    void on_twGraphMatrix_cellChanged(int row, int column);

private:
    bool goodVerticesNumber_;
    size_t rowAndColumnCount_;
    QStringList titles_;
    Ui::MainWindow *ui;
    GraphWidget* graphPropeties_;
    void AddImageHint();
    void SetTableHeaders(QStringList &);
    void CreateTable();
    void setCellCheckBox(QTableWidget *);
};



#endif // MAINWINDOW_HPP
