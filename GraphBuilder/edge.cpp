#include "edge.hpp"


Edge::Edge(Node *source, Node *destination,bool isDirected)
    : arrowSize_(12),isDirected_(isDirected)
{
    setAcceptedMouseButtons(nullptr);
    source_ = source;
    destination_ = destination;
    source_->addEdge(this);
    destination_->addEdge(this);
    adjust();
}

void Edge::adjust()//for connection between nodes
{
    if (!source_ || !destination_)
        return;

    QLineF line(mapFromItem(source_, 0, 0), mapFromItem(destination_, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(20.))
    {
        QPointF edgeOffset((line.dx() * 30) / length, (line.dy() * 30) / length);
        sourcePoint_ = line.p1() + edgeOffset;
        destinationPoint_ = line.p2() - edgeOffset;
    } else
    {
        sourcePoint_ = destinationPoint_ = line.p1();
    }
}

QRectF Edge::boundingRect() const
{
    if (!source_ || !destination_)
        return QRectF();
    qreal penWidth = 1;
       qreal extra = (penWidth + arrowSize_) / 2.0;
    return QRectF(sourcePoint_, QSizeF(destinationPoint_.x() - sourcePoint_.x(),
                                       destinationPoint_.y() - sourcePoint_.y())).normalized()
            .adjusted(-extra, -extra, extra, extra);

}

void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!source_ || !destination_)
        return;

    QLineF line(sourcePoint_, destinationPoint_);
    if (qFuzzyCompare(line.length(), qreal(0.)))
        return;

    // Draw the line
    painter->setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawLine(line);

    // Draw the arrows
     if(isDirected_)
        drawArrows(painter,line);

}

void Edge::drawArrows(QPainter *painter,QLineF &line)
{
    double angle = std::atan2(-line.dy(), line.dx());

    QPointF destArrowP1 = destinationPoint_ + QPointF(sin(angle - M_PI / 3) * arrowSize_,
                                              cos(angle - M_PI / 3) * arrowSize_);
    QPointF destArrowP2 = destinationPoint_ + QPointF(sin(angle - M_PI + M_PI / 3) * arrowSize_,
                                              cos(angle - M_PI + M_PI / 3) * arrowSize_);

    painter->setBrush(Qt::black);
    painter->drawPolygon(QPolygonF() << line.p2() << destArrowP1 << destArrowP2);
}
Node *Edge::sourceNode() const
{
    return source_;
}

Node *Edge::destNode() const
{
    return destination_;
}

void Edge::setDirection(bool direction)
{
    isDirected_=direction;
    update();
}
